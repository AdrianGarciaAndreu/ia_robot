(deffacts datos
	(fila 1 columna 1 2 3 5 6 7 8)
	(fila 2 columna 1 2 3 4 5 6 7 8)
	(fila 3 columna 2 3 7 8)
	(fila 4 columna 1 2 3 5 6 7 8)
	(fila 5 columna 1 2 3 5 6 7 8)
	(paquete fila 4 columna 5 destino 1)
	(paquete fila 5 columna 6 destino 2)
	(almacen 1 fila 1 columna 3 paquetes_restantes 1)
	(almacen 2 fila 3 columna 3 paquetes_restantes 1)
	(visitada fila 0 columna 0 paquete 0 paquetes_restantes 2)
	(ultima visitada fila 0 columna 0)
	(robot fila 4 columna 1 paquetes_restantes 2 paquetes 0 destino 0 deadEnd 0 )

)


;---- Cuando se alcanza una posición de no retorno se vuelve a la casilla previa
(defrule mover_anterior
(declare (salience 75))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)
	
	(test(= ?de 0))
	=>
	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila ?lv_fila columna ?lv_columna paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 1))
)


;aunque se haya visitado, se vuelve a esa casilla para reexplorar otros caminos
(defrule mover_arriba_reexplorar
(declare (salience 50))

	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?f_r columna $?col_ini ?r_col $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)

	(test(= ?de 1))

	(test (and (= (- ?r_fila 1) ?f_r) (<> (- ?r_fila 1) ?lv_fila) ))

	=>

	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila (- ?r_fila 1) columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 1)) ;Se actualiza el robot

)

;------- Mover hacia abajo
(defrule mover_abajo_reexplorar
(declare (salience 50))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?f_r columna $?col_ini ?r_col $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna )

	(test(= ?de 1))

	(test (and (= (+ ?r_fila 1) ?f_r) (<> (+ ?r_fila 1) ?lv_fila) ))
	=>

	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila (+ ?r_fila 1) columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 1))
)


;------- Mover hacia la derecha
(defrule mover_derecha_reexplorar
(declare (salience 50))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?r_fila columna $?col_ini ?r_column $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)

	(test(= ?de 1))

	(test (and (= (+ ?r_col 1) ?r_column) (<> (+ ?r_col 1) ?lv_columna) ))
	=>

	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila ?r_fila columna(+ ?r_col 1) paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 1))	
)


;------- Mover hacia la izquierda
(defrule mover_izquierda_reexplorar
(declare (salience 50))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?r_fila columna $?col_ini ?r_column $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)

	(test(= ?de 1))

	(test (and (= (- ?r_col 1) ?r_column) (<> (- ?r_col 1) ?lv_columna) ))
	=>

	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila ?r_fila columna(- ?r_col 1) paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 1))	
)







;--------------------------------------------------------
;------- MOVIMIENTOS ADYACENTES -------------------------
;--------------------------------------------------------

(defrule mover_arriba
(declare (salience 100))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?f_r columna $?col_ini ?r_col $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)


	(test (= (- ?r_fila ?f_r) 1))
	(not (visitada fila ?f_r columna ?r_col paquete ?p paquetes_restantes ?pr ))

	=>

	(assert (visitada fila (- ?r_fila 1) columna ?r_col paquete ?p paquetes_restantes ?pr )) ;Se registra como visitada la casilla a la que me desplazo
	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila (- ?r_fila 1) columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 0)) ;Se actualiza el robot
)

;------- Mover hacia abajo
(defrule mover_abajo
(declare (salience 100))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?f_r columna $?col_ini ?r_col $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna )


	(test (= (+ ?r_fila 1) ?f_r))
	(not (visitada fila ?f_r columna ?r_col paquete ?p paquetes_restantes ?pr))
	=>

	(assert (visitada fila (+ ?r_fila 1) columna ?r_col paquete ?p paquetes_restantes ?pr))
	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila (+ ?r_fila 1) columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 0))
)


;------- Mover hacia la derecha
(defrule mover_derecha
(declare (salience 100))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?r_fila columna $?col_ini ?r_column $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)

	(test (= (+ ?r_col 1) ?r_column))
	(not (visitada fila ?r_fila columna ?r_column paquete ?p paquetes_restantes ?pr))
	=>

	(assert (visitada fila ?r_fila columna (+ ?r_col 1) paquete ?p paquetes_restantes ?pr))
	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila ?r_fila columna(+ ?r_col 1) paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 0))	
)


;------- Mover hacia la izquierda
(defrule mover_izquierda
(declare (salience 100))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(fila ?r_fila columna $?col_ini ?r_column $?col_fin)
	?lv <- (ultima visitada fila ?lv_fila columna ?lv_columna)


	(test (= (- ?r_col ?r_column) 1))
	(not (visitada fila ?r_fila columna ?r_column paquete ?p paquetes_restantes ?pr))
	=>

	(assert (visitada fila ?r_fila columna (- ?r_col 1) paquete ?p paquetes_restantes ?pr))
	(retract ?lv)
	(assert (ultima visitada fila ?r_fila columna ?r_col)) ;Se guarda la ultima posicion desde la que se mueve

	(retract ?r)
	(assert (robot fila ?r_fila columna(- ?r_col 1) paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd 0))	
)






(defrule recoger_paquete
(declare (salience 150))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	?paq <- (paquete fila ?p_fila columna ?p_columna destino ?destinoPaquete)
	
	(test (= ?p 0))
	(test (and (= ?r_fila ?p_fila)(= ?r_col ?p_columna)))
	=>
	
	(retract ?r)
	(assert (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes (+ ?p 1) destino ?destinoPaquete deadEnd 0)) ;Se suma un paquete al robot
	(retract ?paq)

)



;----- Si el robot tiene un paquete y esta en la casa, lo entrega
(defrule entregar_paquete
(declare (salience 150))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	?casa <- (almacen ?destino fila ?r_fila columna ?r_col paquetes_restantes ?restantes) ;Si estoy sobre el almacen al que corresponde el paquete que llevo encima
	(test (and (= ?p 1) (> ?restantes 0)))
	=>
	(retract ?r)
	(assert (robot fila ?r_fila columna ?r_col paquetes_restantes (- ?pr 1) paquetes 0 destino 0 deadEnd 0))	
	
	(retract ?casa)
	(assert (almacen ?destino fila ?r_fila columna ?r_col paquetes_restantes (- ?restantes 1)))

)


;--- Si todo esta entregado se limpia y la base de hechos
(defrule todo_entregado
(declare (salience 200))
	?r <- (robot fila ?r_fila columna ?r_col paquetes_restantes ?pr paquetes ?p destino ?destino deadEnd ?de)
	(test (= ?pr 0))
	=>
	(clear)
	(halt)
)
